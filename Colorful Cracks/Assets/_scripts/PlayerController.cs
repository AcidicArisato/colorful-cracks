﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	float speed = 3.5f;

	Vector2 playerPos = new Vector2();
	Transform downPulseLoc;
	Transform leftPulseLoc;
	Transform rightPulseLoc;
	Transform upPulseLoc;
	public GameObject downPulse;
	public GameObject leftPulse;
	public GameObject pingWave;
	public GameObject rightPulse;
	public GameObject upPulse;

	// Update is called once per frame
	void Update () {

		Debug.Log (transform.position);
		playerPos = transform.position;

		movement ();

		if (Input.GetKeyDown (KeyCode.Mouse0)){

			horizPulse();

		}
		if (Input.GetKeyDown (KeyCode.Mouse1)) {

			vertiPulse();

		}

	}

	void movement () {

		if (Input.GetKey (KeyCode.D)) {

			transform.Translate (speed * Time.deltaTime, 0, 0);

		}
		if (Input.GetKey (KeyCode.A)) {

			transform.Translate (-(speed * Time.deltaTime), 0, 0);

		}
		if (Input.GetKey (KeyCode.W)) {

			transform.Translate (0, speed * Time.deltaTime, 0);

		}
		if (Input.GetKey (KeyCode.S)) {

			transform.Translate (0, -(speed * Time.deltaTime), 0);

		}

	}

	void horizPulse () {

		//Vector2 hPlayerPos = new Vector2();
		//hPlayerPos = transform.position;

		Debug.Log ("Spawning Horizontal Pulse Wave");
		Instantiate (rightPulse, playerPos, transform.rotation);
		Instantiate (leftPulse, playerPos, transform.rotation);

	}

	void vertiPulse () {

		Debug.Log ("Spawning Vertical Pulse Wave");
		Instantiate (upPulse, playerPos, transform.rotation);
		Instantiate (downPulse, playerPos, transform.rotation);

	}
}
