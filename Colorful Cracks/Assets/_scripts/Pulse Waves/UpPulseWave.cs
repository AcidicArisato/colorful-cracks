﻿using UnityEngine;
using System.Collections;

public class UpPulseWave : MonoBehaviour {

	private Vector2 startPos;
	private Vector2 endPos = new Vector2 (0, 3);

	private float timer;

	void Start () {
	
		startPos = transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		
		timer += Time.deltaTime;
		transform.position = Vector2.Lerp (startPos, endPos, timer);

		if (transform.position == endPos)
			Destroy (gameObject);

	}
}
